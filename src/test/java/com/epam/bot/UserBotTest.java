package com.epam.bot;

import com.epam.sdk.Board;
import com.epam.sdk.BoardCoordinate;
import com.epam.sdk.Orientation;
import com.epam.sdk.Square;
import com.epam.sdk.figures.Pharaoh;
import com.epam.sdk.move.Move;
import com.epam.sdk.move.Shift;
import junit.framework.TestCase;

import static com.epam.sdk.Color.RED;
import static com.epam.sdk.Color.WHITE;

public class UserBotTest extends TestCase {

    public void testNextMove() throws Exception {
        UserBot userBot = new UserBot();
//        assertNull(userBot.nextMove(new Board(), RED));
        String avaliblesMoves[] = {"0001", "0010", "00-1", "00+1", "0011"};
//        String move = userBot.nextMove(Board.createBoardFromPosition("PH 74, S1 79, C1 44, C2 45, A1 73, A1 75, P1 23, P1 42, P1 72, P2 67, P4 32, P4 49, ph 05, s3 00, c1 35, c2 34, a3 04, a3 06, p2 30, p2 47, p3 07, p3 37, p3 40, p3 56, p4 12"), RED);

//        for (int i = 0; i < 20000; i++) {

//        String move = userBot.nextMove(Board.createBoardFromPosition("s3 00, C1 05, P4 09, c2 10, C1 15, ph 17, p3 18, p3 23, p4 27, P4 36, c2 38, P4 41, p1 47, P2 57, A1 58, P3 62, P1 72, A4 74, PH 75, S1 79"), RED);
//        String move2 = userBot.nextMove(Board.createBoardFromPosition("s2 00, p4 02, ph 05, a3 06, p3 07, c2 25, p2 30, c1 35, P1 39, p3 40, C2 44, P4 49, C2 54, P2 57, P2 72, PH 74, A1 75, S1 79"), WHITE);
//        for (int i = 0; i < 10000; i++) {
//            String move3 = userBot.nextMove(Board.createBoardFromPosition("s3 00, a3 04, ph 05, a3 06, p3 07, p4 13, P3 23, c2 35, P4 39, p2 40, c1 44, C1 45, C1 54, p3 60, P1 72, A1 73, PH 74, A1 75, P2 76, S4 79"), RED);
//            if (move3.equals("4030")) {
//                throw new RuntimeException();
//            }
//        }

        //Sheldon
        for (int i = 0; i < 100; i++) {
//            Shift move = new Shift(new BoardCoordinate(3, 3), new BoardCoordinate(2,4));
            long start = System.nanoTime();
            String move = userBot.nextMove(Board.createBoardFromPosition("s3 00, ph 05, C2 16, P4 19, c1 21, p3 23, p3 26, p2 30, P4 32, c1 35, P1 44, P4 48, C2 56, p2 61, PH 74, A1 75, P2 77, S1 79"), WHITE);
            long finish = System.nanoTime();
            System.out.println(finish-start);
            System.out.println(move);
        }
        //            if (move.equalsIgnoreCase("23+1")) {
//                throw new RuntimeException();
//            }
//        }

//        System.out.println(move3);


//        String move = userBot.nextMove(Board.createBoardFromPosition("PH 74, S1 79, C1 44, C2 45, A1 73, A1 75, P1 23, P2 67, P4 32, P4 49, ph 05, s3 00, c1 35, c2 34, a3 04, a3 06, p2 30, p2 47, p3 56, p4 12"), RED);
//        assertInArray(move, avaliblesMoves);


    }

    private void assertInArray(Object move, Object[] avaliblesMoves) {
        for (Object possMoves : avaliblesMoves) {
            if (move.equals(possMoves)) {
                return;
            }
        }
        assertTrue(false);

    }
}