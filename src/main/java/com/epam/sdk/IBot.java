package com.epam.sdk;

/**
 * Created by numitus on 11/10/14.
 */
public interface IBot {
    String nextMove(final Board b, final Color c);
}
