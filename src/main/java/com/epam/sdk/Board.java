package com.epam.sdk;

import com.epam.sdk.figures.*;
import com.epam.sdk.move.Move;
import com.epam.sdk.move.Rotate;
import com.epam.sdk.move.Shift;
import org.apache.commons.lang3.SerializationUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.epam.sdk.Color.*;
import static com.epam.sdk.Rotation.CLOCKWISE;
import static com.epam.sdk.Rotation.COUNTERCLOCKWISE;


/**
 * Class board implements game board with standart size 8x10.
 */
public class Board implements Serializable {
    private static final int DEFAULT_BOARD_WIDTH = 10, DEFAULT_BOARD_HEIGHT = 8;

    private final Square[][] squares = new Square[DEFAULT_BOARD_HEIGHT][DEFAULT_BOARD_WIDTH];
    private final int width = DEFAULT_BOARD_WIDTH;
    private final int height = DEFAULT_BOARD_HEIGHT;

    public Board() {
        initBoardColor();
    }

    /**
     * This method build raw data and build board.
     *
     * @param rawBoardState It is String, with describe board state.
     *                      Example: PH 74, S1 79, C1 44, C2 45
     * @return Board witch matches input String
     */
    public static Board createBoardFromPosition(String rawBoardState) {
        Board board = new Board();
        //We skip all space symbols and split string. All this parts need to have 4 symbols.
        String[] figures = rawBoardState.replaceAll("\\s", "").split(",");

        for (String rawFigure : figures) {
            assert rawFigure.length() == 4;
            //raw figure describe on figure on the board.
            //example: ph00
            //rawFigure  have two parts.
            String figurePart = rawFigure.substring(0, 2); //first part describe figure state [FigureType,Orientation]
            String coordinatePart = rawFigure.substring(2, 4); //second part describe figure coordinate [X,Y]

            BoardCoordinate bc = BoardCoordinate.parse(coordinatePart);
            board.getSquare(bc).figure = FigureFactory.build(figurePart);
        }
        return board;
    }

    /**
     * This method creates squares and fill its by specific color.
     * Color pattern defined in game rules
     */
    private void initBoardColor() {
        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {
                Color squareColor;
                if (x == 0 || (x == 8 && (y == 0 || y == 7))) {
                    squareColor = RED;
                } else if (x == 9 || (x == 1 && (y == 0 || y == 7))) {
                    squareColor = WHITE;
                } else {
                    squareColor = NEUTRAL;
                }
                squares[y][x] = new Square(squareColor);
            }
        }
    }

    public Square getSquare(BoardCoordinate bc) {
        return squares[bc.getY()][bc.getX()];
    }

    /**
     * Return true if coordinate out of bounds.
     */
    public boolean isOutOfBounds(BoardCoordinate bc) {
        return bc.getX() < 0 || bc.getX() >= getWidth() || bc.getY() < 0 || bc.getY() >= getHeight();
    }


    /**
     * @param figure Figure object
     * @return Coordinate of this object on the board.
     * WARING!!! This method compare figures by reference
     */
    public BoardCoordinate getFigureCoordinates(Figure figure) {
        for (int x = 0; x < getWidth(); x++) {
            for (int y = 0; y < getHeight(); y++) {
                BoardCoordinate boardCoordinate = new BoardCoordinate(x, y);
                if (getSquare(boardCoordinate).figure == figure) {
                    return boardCoordinate;
                }
            }
        }

        return null;
    }

    /**
     * Return figure witch will be killed by laser witch is located in rayCoordinate and have rayDirection.
     * If ray will not kill any figure, method return null
     */
    public Figure getFigureToBeKilledByLaser(BoardCoordinate rayCoordinate, Orientation rayDirection) {
        if (isOutOfBounds(rayCoordinate)) {
            return null;
        }

        Figure figure = getSquare(rayCoordinate).figure;
        if (figure != null) {
            if (figure.isKilledByLaser(rayDirection)) {
                return figure;
            }
            if (figure.isReflectLaser(rayDirection)) {
                //Laser ray receives new orientation
                Orientation newOrientation = figure.getDirectionOfReflectedLaser(rayDirection);
                return getFigureToBeKilledByLaser(rayCoordinate.add(newOrientation), newOrientation);
            }

            return null;
        } else {
            //Otherwise do one step in current direction
            return getFigureToBeKilledByLaser(rayCoordinate.add(rayDirection), rayDirection);
        }
    }

    public Figure getFigureToBeKilledBySphinx(Color myColor) {
        Figure mySphinx = getMySphinx(myColor);
        if (mySphinx == null) {
            return null;
        }
        //If my sphinx is live, I send ray.
        BoardCoordinate sphinxCoordinate = getFigureCoordinates(mySphinx);
        BoardCoordinate startLaserRayCoordinate = sphinxCoordinate.add(mySphinx.orientation);
        return getFigureToBeKilledByLaser(startLaserRayCoordinate, mySphinx.orientation);
    }

    private Figure getMySphinx(Color myColor) {
        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {
                BoardCoordinate boardCoordinate = new BoardCoordinate(x, y);
                Figure figure = getSquare(boardCoordinate).figure;

                if (figure instanceof Sphinx && figure.color == myColor) {
                    return figure;
                }
            }
        }
        return null;
    }

    public boolean isMoveValid(Rotate rotate, Color myColor) {
        if (isOutOfBounds(rotate.getOrigin())) {
            return false;
        }

        Figure figure = getSquare(rotate.getOrigin()).figure;
        return figure != null && figure.color == myColor;

    }

    public boolean isMoveValid(Shift shift, Color myColor) {
        if (isOutOfBounds(shift.origin) || isOutOfBounds(shift.destination)) {
            return false;
        }

        Square origin = getSquare(shift.origin);
        Figure figure = origin.figure;

        if (figure == null) {
            return false;
        }

        if (figure.color != myColor || figure instanceof Sphinx) {
            return false;
        }

        if (Math.abs(shift.origin.getX() - shift.destination.getX()) > 1
                || Math.abs(shift.origin.getY() - shift.destination.getY()) > 1
                || shift.origin.equals(shift.destination)) {
            return false;
        }

        Square destination = getSquare(shift.destination);
        if (!destination.isAvailable(figure.color)) {
            return false;
        }

        Figure destinationFigure = destination.figure;
        if (destinationFigure == null) {
            return true;
        }

        /*
            If destination cell isn't empty,
            only Scarab can be swapped with Pyramid and Anubis.
         */
        return figure instanceof Scarab
                && (destinationFigure instanceof Pyramid || destinationFigure instanceof Anubis)
                && origin.isAvailable(destinationFigure.color);
    }


    /**
     * This method check all possible moves of all my figures, and return available.
     */
    public List<Move> getAvailableMoves(Color myColor) {
        List<Move> list = new ArrayList<>();

        for (int x = 0; x < getWidth(); x++) {
            for (int y = 0; y < getHeight(); y++) {
                BoardCoordinate origin = new BoardCoordinate(x, y);
                Figure figure = getSquare(origin).figure;
                if (figure != null && figure.color == myColor) {
                    list.add(new Rotate(origin, CLOCKWISE));
                    list.add(new Rotate(origin, COUNTERCLOCKWISE));

                    for (int dx = -1; dx <= 1; dx++) {
                        for (int dy = -1; dy <= 1; dy++) {
                            Shift move = new Shift(origin, new BoardCoordinate(origin.getX() + dx, origin.getY() + dy));
                            if (isMoveValid(move, myColor)) {
                                list.add(move);
                            }
                        }
                    }
                }
            }
        }
        return list;
    }

    /**
     * This method apply move figure rotation, and change board state.
     */
    public void makeMove(Move move, Color myColor) {
        if (move instanceof Rotate) {
            Rotate rotate = (Rotate) move;
            if (!isMoveValid(rotate, myColor)) {
                throw new RuntimeException(String.format("Not valid move:%s", rotate));
            }
            getSquare(rotate.getOrigin()).figure.doTurn(rotate.getRotation());

        } else {
            Shift shift = (Shift) move;
            if (!isMoveValid(shift, myColor)) {
                throw new RuntimeException(String.format("Not valid move:%s", shift));
            }
            Square origin = getSquare(shift.origin);
            Square destination = getSquare(shift.destination);
        /*
            We swap figures from origin and destination. If destination cell is empty, we swap origin figure with null.
         */
            Figure temp = origin.figure;
            origin.figure = destination.figure;
            destination.figure = temp;
        }
    }

    public Board clone() {
        Board board = SerializationUtils.clone(this);
        //      BeanUtils.copyProperties(this,board);
       /*     for (int y = 0; y < getHeight(); y++) {
                for (int x = 0; x < getWidth(); x++) {
                    board.squares[y][x]=squares[y][x].clone();

                }
            }*/
        return board;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
