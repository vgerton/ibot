package com.epam.runner;

import com.epam.bot.DefaultBot;
import com.epam.bot.UserBot;
import com.epam.sdk.Color;
import com.epam.sdk.GameState;
import com.epam.sdk.IBot;
import org.apache.camel.Exchange;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.epam.sdk.Color.WHITE;


/**
 * This class run bot.
 */
public class Runner {
    public static final Color USER_BOT_COLOR = WHITE;

    final List<IBot> bots = new ArrayList<>();
    final int botCount;
    int currentTurn = 0;

    Runner() {
        bots.add(new UserBot());
        bots.add(new DefaultBot());
        botCount = isLocaleMode() ? 2 : 1;
    }

    private boolean isLocaleMode() {
        return Boolean.valueOf(System.getProperty("IS_IN_LOCAL_MODE", "true"));
    }

    /**
     * Method switch bot, and build move from current bot.
     * This method runs camel.
     */
    @SuppressWarnings("unchecked")
    public String getMove(Exchange exchange) {
        Map<String, Object> body = (Map) exchange.getIn().getBody(); //Raw data from server
        GameState gameState = GameState.parse(body);

        String currentMove = bots.get(currentTurn).nextMove(gameState.getBoard(), gameState.getColor());
        //Choose other bot
        currentTurn = (currentTurn + 1) % botCount;
        return currentMove;
    }
}
