package com.epam.bot;

import com.epam.sdk.*;
import com.epam.sdk.figures.*;
import com.epam.sdk.move.Move;
import com.epam.sdk.move.Rotate;
import com.epam.sdk.move.Shift;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.epam.sdk.Color.*;
import static com.epam.sdk.Rotation.CLOCKWISE;
import static com.epam.sdk.Rotation.COUNTERCLOCKWISE;


/**
 * User bot implementation.
 * You need to implements your logic in method nextMove().
 * You can build example in class DefaultBot
 */
public class UserBot implements IBot {
    private static final Logger LOG = LoggerFactory.getLogger(UserBot.class);

    private static int numberOfMyFigures = 13;
    private static int numberOfEnemyFigures = 13;

    private static State state = State.FIRST;

    private static Color myColor;
    private static Color rivalColor;
    private static boolean firstMove = true;

    private static int maxScore = 0;

    private int anubisScore = 1850;
    private int pyramidScore = 1000;
    private int pharaohScore = 2500;

    private static int stepCount = 0;

    private Random rand = new Random();

    private String currentBoardNotation = null;

    private static int enemyPhX = 0;
    private static int enemyPhY = 0;

    @Override
    public String nextMove(Board b, Color c) {
        final long start = System.nanoTime();

        stepCount++;
        if (firstMove) {
            firstMove = false;
            myColor = c;
            if (myColor.equals(Color.RED)) {
                rivalColor = Color.WHITE;
            } else {
                rivalColor = Color.RED;
                return "6757";
            }
        }

        Move move = getMove(b);

        long finish = System.nanoTime();
        long result = finish - start;

        LOG.info("[UserBot] color: {} move: {} my figures {} enemy figures {} max score {} time {} notation {}\n", c.toString(), move, numberOfMyFigures, numberOfEnemyFigures, maxScore, result, currentBoardNotation);
        if (move == null) {
            return null;
        }

        return move.toString();
    }


    public Move getMove(Board b) {
        MyBoard board = new MyBoard();
        try {
            Field f = b.getClass().getDeclaredField("squares");
            f.setAccessible(true);
            board.setSquares((Square[][]) f.get(b));

        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        countEnemyPharaohCoord(board);

        determineState(board);

        currentBoardNotation = board.getNotation();

        final List<Move> firstMoves = getAvailableMoves(board, myColor, true);

        int firstStepMaxScore = -12700;
        Move bestMove = null;
        InterResult firstStepBestResult = null;

        final List<InterResult> interResults = new ArrayList<>();
        for (final Move firstMove : firstMoves) {
            MyBoard firstStepBoard = board.clone();
            firstStepBoard.makeMove(firstMove, myColor);

            int firstMoveState = getFitness(firstStepBoard, myColor, true, false);

            if (firstMoveState == pharaohScore) {
                maxScore = pharaohScore;
                return firstMove;
            }

            if (firstMoveState == -pharaohScore) {
                if (bestMove == null) {
                    bestMove = firstMove;
                }

                continue;
            }

            final List<Move> secondMoves = getAvailableMoves(firstStepBoard, rivalColor, false);
            int secondStepMinScore = 12700;
            InterResult secondStepMinResult = null;

            for (final Move secondMove : secondMoves) {
                MyBoard secondStepBoard = firstStepBoard.clone();
                secondStepBoard.makeMove(secondMove, rivalColor);
                int secMoveState = getFitness(secondStepBoard, rivalColor, false, false);

                if (secMoveState == -pharaohScore) {
                    secondStepMinScore = secMoveState;
                    break;
                }

                secMoveState += firstMoveState;

                final List<Move> thirdMoves = getAvailableMoves(secondStepBoard, myColor, true);
                int thirdStepMaxScore = -12700;
//                InterResult thirdStepMaxResult = null;

                for (final Move thirdMove : thirdMoves) {
                    MyBoard thirdStepBoard = secondStepBoard.clone();
                    thirdStepBoard.makeMove(thirdMove, myColor);

                    int thCurrentScore = getFitness(thirdStepBoard, myColor, false, true) + secMoveState + rand.nextInt(20);
                    if (thCurrentScore >= thirdStepMaxScore) {
                        thirdStepMaxScore = thCurrentScore;
//                        thirdStepMaxResult = new InterResult(firstMove, thirdStepBoard, thCurrentScore);
                    }

                    if (thirdStepMaxScore > secondStepMinScore) {
                        break;
                    }

                }

                if (thirdStepMaxScore <= secondStepMinScore) {
                    secondStepMinScore = thirdStepMaxScore;
//                    secondStepMinResult = new InterResult(firstMove, thirdStepMaxResult.getBoard(), secondStepMinScore);
                }

                if (secondStepMinScore < firstStepMaxScore) {
                    break;
                }

            }

            if (secondStepMinScore >= firstStepMaxScore) {
                firstStepMaxScore = secondStepMinScore;
                bestMove = firstMove;
            }

//            if (secondStepMinScore > 1000 && secondStepMinResult != null) {
//                interResults.add(new InterResult(firstMove, secondStepMinResult.getBoard(), secondStepMinScore));
//            }

        }

        maxScore = firstStepMaxScore;

//        if (maxScore > 1000 && maxScore < 2200) {
//            Move afterAnalyzeMove = analyzeInterResults(interResults);
//            if (afterAnalyzeMove != null) {
//                bestMove = afterAnalyzeMove;
//            }
//        }

        return bestMove;
    }

    public void makeMove(Board b, Color c) {

        myColor = c;
        if (myColor.equals(Color.RED)) {
            rivalColor = Color.WHITE;
        } else {
            rivalColor = Color.RED;
        }

        MyBoard board = new MyBoard();
        try {
            Field f = b.getClass().getDeclaredField("squares");
            f.setAccessible(true);
            board.setSquares((Square[][]) f.get(b));

        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        Rotate rotate = new Rotate(new BoardCoordinate(9, 3), Rotation.COUNTERCLOCKWISE);

        System.out.println(rotate.toString());
        board.makeMove(rotate, myColor);

        System.out.println();

    }

    public Move analyzeInterResults(final List<InterResult> interResults) {
        int maxScore = -12500;
        Move bestMove = null;
        for (InterResult interResult : interResults) {

            final List<Move> fourthMoves = getAvailableMoves(interResult.getBoard(), rivalColor, false);
            int fourthStepMinScore = 12500;

            for (Move fourthMove : fourthMoves) {
                MyBoard fourthStepBoard = interResult.getBoard().clone();
                fourthStepBoard.makeMove(fourthMove, rivalColor);
                int fourthMoveState = getFitness(fourthStepBoard, rivalColor, false, false) + interResult.getScore();

                final List<Move> fifthMoves = getAvailableMoves(fourthStepBoard, myColor, true);
                int fifthStepMaxScore = -12500;

                for (Move fifthMove : fifthMoves) {
                    MyBoard fifthStepBoard = fourthStepBoard.clone();
                    fifthStepBoard.makeMove(fifthMove, myColor);
                    int fifthMoveState = getFitness(fifthStepBoard, myColor, false, false) + fourthMoveState;

                    if (fifthMoveState > fifthStepMaxScore) {
                        fifthStepMaxScore = fifthMoveState;
                    }

                    if (fifthStepMaxScore > fourthStepMinScore) {
                        break;
                    }
                }

                if (fifthStepMaxScore < fourthStepMinScore) {
                    fourthStepMinScore = fifthStepMaxScore;
                }

                if (fourthStepMinScore < maxScore) {
                    break;
                }

            }

            if (fourthStepMinScore >= maxScore) {
                maxScore = fourthStepMinScore;
                bestMove = interResult.getMove();

            }
        }

        return bestMove;
    }


    public int getFitness(MyBoard board, Color color, boolean neededBoardFitness, boolean thirdStep) {
        int fitness = 0;
        Figure figureToKill = board.getFigureToBeKilledBySphinx(color);

        if (figureToKill != null) {
            if (figureToKill instanceof Pharaoh) {
                fitness = countScore(figureToKill, pharaohScore);
            } else {
                if (figureToKill instanceof Pyramid) {
                    fitness = countScore(figureToKill, pyramidScore);
                } else {
                    if (figureToKill instanceof Anubis) {
                        fitness = countScore(figureToKill, anubisScore);
                    }
                }
            }
        }

        if (thirdStep && fitness > 0) {
            fitness -= 200;
        }

        if (neededBoardFitness && fitness != (pharaohScore + 200)) {
            fitness += getPositionFitness(board);
        }

        if (thirdStep && stepCount>2 ) {
            fitness += getPositionFitness(board);
        }

        return fitness;
    }


    public void countEnemyPharaohCoord(MyBoard board) {
        for (int x = 0; x < board.getWidth(); x++) {
            for (int y = 0; y < board.getHeight(); y++) {
                BoardCoordinate origin = new BoardCoordinate(x, y);
                Figure figure = board.getSquare(origin).figure;

                if (figure != null && figure instanceof Pharaoh && figure.color == rivalColor) {
                    enemyPhX = x;
                    enemyPhY = y;
                    break;
                }
            }
        }
    }

    public int getPositionFitness(MyBoard board) {
        List<MyFigure> scarabs = new ArrayList<>();
        List<MyFigure> pyramids = new ArrayList<>();
        List<MyFigure> anubises = new ArrayList<>();

        int fitness = 0;
        for (int x = 0; x < board.getWidth(); x++) {
            for (int y = 0; y < board.getHeight(); y++) {
                BoardCoordinate origin = new BoardCoordinate(x, y);
                Figure figure = board.getSquare(origin).figure;
                if (figure != null && figure.color == myColor) {
                    if (figure instanceof Scarab) {
                        fitness += getPositionFitnessForScarab(figure, origin, scarabs);
                    } else if (figure instanceof Pyramid) {
                        fitness += getPositionFitnessForPyramid(figure, origin, pyramids);
                    } else if (figure instanceof Pharaoh) {
                        fitness += getPositionFitnessForPharaoh(origin);
                    } else if (figure instanceof Anubis) {
                        fitness += getPositionFitnessForAnubis(origin);
                    }
                }
            }
        }

        return fitness;

    }

    public int getPositionFitnessForScarab(Figure figure, BoardCoordinate origin, List<MyFigure> scarabs) {
        int fitness = 0;

        if (stepCount >= 50) {
            return fitness;
        }

        if (myColor == Color.WHITE) {
            if (origin.getY() == 4 && (checkWhiteScarabX(origin))) {
                fitness += 10;
            } else if (origin.getY() == 6 && origin.getX() == 4) {
                fitness += 70;
            } else if (origin.getY() == 6 && origin.getX() == 5) {
                fitness += 45;
            } else if ((origin.getY() == 3) && (checkWhiteScarabX(origin))) {
                fitness += 30;
            } else if (origin.getY() == 5 && (checkWhiteScarabX(origin))) {
                fitness += 40;
            } else if (origin.getY() == 2 && (checkWhiteScarabX(origin))) {
                fitness += 45;
            } else if (origin.getY() == 1 && (origin.getX() == 4 || origin.getX() == 6)) {
                fitness += 60;

                if (figure.getOrientation() == Orientation.EAST) {
                    fitness += 50;
                }
            } else if (origin.getY() == 1 && origin.getX() == 5) {
                fitness += 70;

                if (figure.getOrientation() == Orientation.EAST) {
                    fitness += 50;
                }
            } else if (origin.getY() == 0 && origin.getX() == 6) {
                fitness += 120;
            }

        } else {
            if (origin.getY() == 3 && (checkRedScarabX(origin))) {
                fitness += 10;
            } else if (origin.getY() == 1 && origin.getX() == 5) {
                fitness += 70;
            } else if (origin.getY() == 1 && origin.getX() == 4) {
                fitness += 45;
            } else if ((origin.getY() == 4) && (checkRedScarabX(origin))) {
                fitness += 30;
            } else if (origin.getY() == 2 && (checkRedScarabX(origin))) {
                fitness += 40;
            } else if (origin.getY() == 5 && (checkRedScarabX(origin))) {
                fitness += 45;
            } else if (origin.getY() == 6 && (origin.getX() == 3 || origin.getX() == 5)) {
                fitness += 60;
                if (figure.getOrientation() == Orientation.EAST) {
                    fitness += 50;
                }
            } else if (origin.getY() == 6 && origin.getX() == 4) {
                fitness += 70;

                if (figure.getOrientation() == Orientation.EAST) {
                    fitness += 50;
                }
            } else if (origin.getY() == 7 && origin.getX() == 3) {
                fitness += 120;
            }


        }

        if (scarabs.size() == 1) {
            MyFigure checkedScarab = scarabs.get(0);
            if ((checkedScarab.getOrigin().getY() >= 4 && origin.getY() >= 4) || (checkedScarab.getOrigin().getY() <= 3 && origin.getY() <= 3)) {
                fitness -= 300;
            }
        }

        if (Math.abs(origin.getY() - enemyPhY) + Math.abs(origin.getX() - enemyPhX) <= 1) {
            fitness += 180;
        } else if (Math.abs(origin.getY() - enemyPhY) + Math.abs(origin.getX() - enemyPhX) == 2) {
            fitness += 120;
        }

        scarabs.add(new MyFigure(origin));

        return fitness;
    }

    public int getPositionFitnessForPyramid(Figure figure, BoardCoordinate origin, List<MyFigure> pyramids) {
        int fitness = 0;

        if (myColor == Color.WHITE) {
            if (origin.getX() == 8 && (origin.getY() == 1)) {
                fitness += 65;
            } else if (origin.getX() == 8 && (origin.getY() == 2)) {
                fitness += 55;
            } else if (origin.getX() == 8 && (origin.getY() == 3)) {
                fitness += 50;
            } else if (origin.getX() == 8 && (origin.getY() == 4)) {
                fitness += 35;
            } else if (origin.getX() == 8 && origin.getY() == 5) {
                fitness += 20;
            } else if (origin.getY() == 6 && (origin.getX() >= 6 && origin.getX() <= 8)) {
                fitness += 36;
            } else if (origin.getX() == 9 && (origin.getY() == 4)) {
                fitness += 25;
            } else if (origin.getX() == 9 && (origin.getY() == 3)) {
                fitness += 45;
            } else if (origin.getX() == 9 && origin.getY() == 2) {
                fitness += 60;
                if (figure.orientation == Orientation.WEST) {
                    fitness += 100;
                }
            } else if (origin.getX() == 9 && (origin.getY() == 1)) {
                fitness += 70;
                if (figure.orientation == Orientation.WEST) {
                    fitness += 100;
                }
            } else if (origin.getX() == 9 && origin.getY() == 0) {
                fitness += 70;
                if (figure.orientation == Orientation.WEST) {
                    fitness += 100;
                }
            } else if (origin.getX() == 9 && origin.getY() == 5) {
                fitness += 20;
            } else if (origin.getY() == 7 && (origin.getX() >= 6 && origin.getX() <= 7)) {
                fitness += 45;
            } else if (origin.getY() == 7 && origin.getX() == 8) {
                fitness += 50;
            } else if (origin.getY() == 7 && (origin.getX() >= 0 && origin.getY() <= 1)) {
                fitness += 15;
            } else if (origin.getY() == 7 && origin.getX() == 2) {
                fitness += 30;
            }

//            if (stepCount >= 5) {
//                if (origin.getY() >= 5 && origin.getX() == 9) {
//                    fitness -= 300;
//                }
//            }

        } else {
            if (origin.getX() == 1 && (origin.getY() == 6)) {
                fitness += 65;
            } else if (origin.getX() == 1 && origin.getY() == 5) {
                fitness += 55;
            } else if (origin.getX() == 1 && origin.getY() == 4) {
                fitness += 50;
            } else if (origin.getX() == 1 && (origin.getY() == 3)) {
                fitness += 35;
            } else if (origin.getX() == 1 && (origin.getY() == 2)) {
                fitness += 20;
            } else if (origin.getY() == 1 && (origin.getX() >= 1 && origin.getX() <= 3)) {
                fitness += 36;
            } else if (origin.getX() == 0 && (origin.getY() == 3)) {
                fitness += 25;
            } else if (origin.getX() == 0 && origin.getY() == 4) {
                fitness += 45;
            } else if (origin.getX() == 0 && origin.getY() == 5) {
                fitness += 60;
                if (figure.getOrientation() == Orientation.EAST) {
                    fitness += 100;
                }
            } else if (origin.getX() == 0 && (origin.getY() == 6)) {
                fitness += 70;
                if (figure.getOrientation() == Orientation.EAST) {
                    fitness += 100;
                }
            } else if (origin.getX() == 0 && origin.getY() == 7) {
                fitness += 70;
                if (figure.getOrientation() == Orientation.EAST) {
                    fitness += 100;
                }
            } else if (origin.getX() == 0 && origin.getY() == 2) {
                fitness += 20;

            } else if (origin.getY() == 0 && (origin.getX() >= 2 && origin.getX() <= 3)) {
                fitness += 45;
            } else if (origin.getY() == 0 && origin.getX() == 1) {
                fitness += 50;
            } else if (origin.getY() == 0 && (origin.getX() >= 8 && origin.getX() <= 9)) {
                fitness += 15;
            } else if (origin.getY() == 0 && origin.getX() == 7) {
                fitness += 30;
            }

//            if (stepCount >= 5) {
//                if (origin.getX() == 0 && origin.getY() <= 4) {
//                    fitness -= 300;
//                }
//            }

        }

        pyramids.add(new MyFigure(origin));

        return fitness;
    }

    public int getPositionFitnessForPharaoh(BoardCoordinate origin) {
        int fitness = 0;

        if (myColor == Color.WHITE) {
            if (origin.getY() == 7 && origin.getX() == 4) {
                fitness += 100;
            }
        } else {
            if (origin.getY() == 0 && origin.getX() == 5) {
                fitness += 100;
            }
        }

        return fitness;
    }

    public int getPositionFitnessForAnubis(BoardCoordinate origin) {
        int fitness = 0;

        if (myColor == Color.WHITE) {
            if (origin.getY() == 7 && (origin.getX() == 3 || origin.getX() == 5)) {
                fitness += 100;
            }
        } else {
            if (origin.getY() == 0 && (origin.getX() == 4 || origin.getX() == 6)) {
                fitness += 100;
            }
        }

        return fitness;
    }

    private boolean checkWhiteScarabX(BoardCoordinate origin) {
        return origin.getX() >= 4 && origin.getX() <= 6;
    }

    private boolean checkRedScarabX(BoardCoordinate origin) {
        return origin.getX() >= 3 && origin.getX() <= 5;
    }

    private boolean isEnemy(Figure figureToKill, Color color) {
        return figureToKill != null && figureToKill.isEnemyColor(color);
    }

    private int countScore(Figure figureToKill, int scoreConst) {
        int score = 0;

        if (figureToKill.color == myColor) {
            score -= scoreConst;
        } else {
            score += scoreConst;
        }

        return score;
    }

    public List<Move> getAvailableMoves(MyBoard board, Color myColor, boolean myStep) {
        List<Move> list = new ArrayList<>();
        Rotation[] rotations = {Rotation.CLOCKWISE, Rotation.COUNTERCLOCKWISE};

        for (int x = 0; x < board.getWidth(); x++) {
            for (int y = 0; y < board.getHeight(); y++) {
                BoardCoordinate origin = new BoardCoordinate(x, y);
                Figure figure = board.getSquare(origin).figure;
                if (includeFigureInAnalyze(figure, myColor, origin, myStep)) {
                    for (Rotation rot : rotations) {
                        Rotate r = new Rotate(origin, rot);
                        if (board.isMoveValid(r, myColor)) {
                            list.add(r);
                        }
                    }

                    for (int dx = -1; dx <= 1; dx++) {
                        for (int dy = -1; dy <= 1; dy++) {
                            Shift move = new Shift(origin, new BoardCoordinate(origin.getX() + dx, origin.getY() + dy));
                            if (board.isMoveValid(move, myColor)) {
                                list.add(move);
                            }
                        }
                    }
                }
            }
        }
        return list;
    }

//    private boolean includeFigureInAnalyze(Figure figure, Color myColor, BoardCoordinate origin, boolean myStep) {
//        if (numberOfMyFigures + numberOfEnemyFigures > 25) {
//            return figure != null && figure.color == myColor && figure instanceof Pyramid && origin.getX() != 0 && origin.getX() != 7 && origin.getY()!=9;
//        } else if (numberOfMyFigures + numberOfEnemyFigures > 24) {
//            return figure != null && figure.color == myColor && figure instanceof Pyramid && origin.getX() != 0 && origin.getX() != 7;
//        } else if (numberOfMyFigures + numberOfEnemyFigures > 21) {
//            return figure != null && figure.color == myColor && ((figure instanceof Pyramid));
//        } else if (numberOfMyFigures + numberOfEnemyFigures > 17) {
//            return figure != null && figure.color == myColor && ((figure instanceof Pyramid || figure instanceof Scarab));
//        } else {
//            return figure != null && figure.color == myColor;
//        }
//    }


    private boolean includeFigureInAnalyze(Figure figure, Color myColor, BoardCoordinate origin, boolean myStep) {
        switch (state) {
            case FIRST:
                return figure != null && figure.color == myColor && ((figure instanceof Pyramid || figure instanceof Scarab || figure instanceof Sphinx || isMyAnubis(figure)));
            case SECOND:
                return figure != null && figure.color == myColor;
        }

        return false;
    }

    private boolean isRivalSphinx(Figure figure) {
        if (figure instanceof Sphinx && figure.color == rivalColor) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isMySphinx(Figure figure) {
        if (figure instanceof Sphinx && figure.color == myColor) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isMyAnubis(Figure figure) {
        if (figure instanceof Anubis && figure.color == myColor) {
            return true;
        } else {
            return false;
        }
    }

    private void determineState(MyBoard board) {
        numberOfMyFigures = board.getFigureCount(myColor);
        numberOfEnemyFigures = board.getFigureCount(rivalColor);

        if (numberOfMyFigures + numberOfEnemyFigures > 23) {
            state = State.FIRST;
        } else {
            state = State.SECOND;
        }
    }
}

class MyFigure {
    private Figure figure;
    private BoardCoordinate origin;

    public MyFigure(BoardCoordinate coordinate) {
        this.origin = coordinate;
    }

    public Figure getFigure() {
        return figure;
    }

    public void setFigure(Figure figure) {
        this.figure = figure;
    }

    public BoardCoordinate getOrigin() {
        return origin;
    }

    public void setOrigin(BoardCoordinate origin) {
        this.origin = origin;
    }
}

class InterResult {
    private Move move;
    private MyBoard board;
    private int score;


    public InterResult(Move move, MyBoard board, int score) {
        this.move = move;
        this.board = board;
        this.score = score;
    }

    public Move getMove() {
        return move;
    }

    public void setMove(Move move) {
        this.move = move;
    }

    public MyBoard getBoard() {
        return board;
    }

    public void setBoard(MyBoard board) {
        this.board = board;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}

enum State {
    FIRST, SECOND, THIRD, FOURTH;
}

class MyBoard {
    private static final int DEFAULT_BOARD_WIDTH = 10, DEFAULT_BOARD_HEIGHT = 8;

    private Square[][] squares = new Square[DEFAULT_BOARD_HEIGHT][DEFAULT_BOARD_WIDTH];
    private final int width = DEFAULT_BOARD_WIDTH;
    private final int height = DEFAULT_BOARD_HEIGHT;

    public MyBoard() {
        initBoardColor();
    }

    /**
     * This method build raw data and build board.
     *
     * @param rawBoardState It is String, with describe board state.
     *                      Example: PH 74, S1 79, C1 44, C2 45
     * @return Board witch matches input String
     */
    public static MyBoard createBoardFromPosition(String rawBoardState) {
        MyBoard board = new MyBoard();
        //We skip all space symbols and split string. All this parts need to have 4 symbols.
        String[] figures = rawBoardState.replaceAll("\\s", "").split(",");

        for (String rawFigure : figures) {
            assert rawFigure.length() == 4;
            //raw figure describe on figure on the board.
            //example: ph00
            //rawFigure  have two parts.
            String figurePart = rawFigure.substring(0, 2); //first part describe figure state [FigureType,Orientation]
            String coordinatePart = rawFigure.substring(2, 4); //second part describe figure coordinate [X,Y]

            BoardCoordinate bc = BoardCoordinate.parse(coordinatePart);
            board.getSquare(bc).figure = FigureFactory.build(figurePart);
        }
        return board;
    }

    /**
     * This method creates squares and fill its by specific color.
     * Color pattern defined in game rules
     */
    private void initBoardColor() {
        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {
                Color squareColor;
                if (x == 0 || (x == 8 && (y == 0 || y == 7))) {
                    squareColor = RED;
                } else if (x == 9 || (x == 1 && (y == 0 || y == 7))) {
                    squareColor = WHITE;
                } else {
                    squareColor = NEUTRAL;
                }
                squares[y][x] = new Square(squareColor);
            }
        }
    }

    public Square getSquare(BoardCoordinate bc) {
        return squares[bc.getY()][bc.getX()];
    }

    public Square[][] getSquares() {
        return squares;
    }

    public void setSquares(Square[][] squares) {
        this.squares = squares;
    }

    /**
     * Return true if coordinate out of bounds.
     */
    public boolean isOutOfBounds(BoardCoordinate bc) {
        return bc.getX() < 0 || bc.getX() >= getWidth() || bc.getY() < 0 || bc.getY() >= getHeight();
    }


    /**
     * @param figure Figure object
     * @return Coordinate of this object on the board.
     * WARING!!! This method compare figures by reference
     */
    public BoardCoordinate getFigureCoordinates(Figure figure) {
        for (int x = 0; x < getWidth(); x++) {
            for (int y = 0; y < getHeight(); y++) {
                BoardCoordinate boardCoordinate = new BoardCoordinate(x, y);
                if (getSquare(boardCoordinate).figure == figure) {
                    return boardCoordinate;
                }
            }
        }

        return null;
    }

    /**
     * Return figure witch will be killed by laser witch is located in rayCoordinate and have rayDirection.
     * If ray will not kill any figure, method return null
     */
    public Figure getFigureToBeKilledByLaser(BoardCoordinate rayCoordinate, Orientation rayDirection) {
        if (isOutOfBounds(rayCoordinate)) {
            return null;
        }

        Figure figure = getSquare(rayCoordinate).figure;
        if (figure != null) {
            if (figure.isKilledByLaser(rayDirection)) {
                Figure newFigure = copyFigure(figure);

                //remove figure from the board
                getSquare(rayCoordinate).figure = null;

                //return figure that will be killed
                return newFigure;
            }
            if (figure.isReflectLaser(rayDirection)) {
                //Laser ray receives new orientation
                Orientation newOrientation = figure.getDirectionOfReflectedLaser(rayDirection);
                return getFigureToBeKilledByLaser(rayCoordinate.add(newOrientation), newOrientation);
            }

            return null;
        } else {
            //Otherwise do one step in current direction
            return getFigureToBeKilledByLaser(rayCoordinate.add(rayDirection), rayDirection);
        }
    }

    public Figure getFigureToBeKilledBySphinx(Color myColor) {
        Figure mySphinx = getMySphinx(myColor);
        if (mySphinx == null) {
            return null;
        }
        //If my sphinx is live, I send ray.
        BoardCoordinate sphinxCoordinate = getFigureCoordinates(mySphinx);
        BoardCoordinate startLaserRayCoordinate = sphinxCoordinate.add(mySphinx.orientation);
        return getFigureToBeKilledByLaser(startLaserRayCoordinate, mySphinx.orientation);
    }

    private Figure getMySphinx(Color myColor) {
        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {
                BoardCoordinate boardCoordinate = new BoardCoordinate(x, y);
                Figure figure = getSquare(boardCoordinate).figure;

                if (figure instanceof Sphinx && figure.color == myColor) {
                    return figure;
                }
            }
        }
        return null;
    }

    public boolean isMoveValid(Rotate rotate, Color myColor) {
        if (isOutOfBounds(rotate.getOrigin())) {
            return false;
        }

        Figure figure = getSquare(rotate.getOrigin()).figure;
        if (figure == null) return false;
        if (figure.color != myColor) return false;
        if (!(figure instanceof Sphinx)) return true;

        Rotation r = rotate.getRotation();
        Orientation o = figure.getOrientation();
        Color c = figure.getColor();

        if (((c == Color.WHITE) && (o == Orientation.NORTH) && (r == Rotation.CLOCKWISE))
                || ((c == Color.WHITE) && (o == Orientation.WEST) && (r == Rotation.COUNTERCLOCKWISE))
                || ((c == Color.RED) && (o == Orientation.SOUTH) && (r == Rotation.CLOCKWISE))
                || ((c == Color.RED) && (o == Orientation.EAST) && (r == Rotation.COUNTERCLOCKWISE))) {
            return false;
        }

        return true;
    }

    public boolean isMoveValid(Shift shift, Color myColor) {
        if (isOutOfBounds(shift.origin) || isOutOfBounds(shift.destination)) {
            return false;
        }

        Square origin = getSquare(shift.origin);
        Figure figure = origin.figure;

        if (figure == null) {
            return false;
        }

        if (figure.color != myColor || figure instanceof Sphinx) {
            return false;
        }

        if (Math.abs(shift.origin.getX() - shift.destination.getX()) > 1
                || Math.abs(shift.origin.getY() - shift.destination.getY()) > 1
                || shift.origin.equals(shift.destination)) {
            return false;
        }

        Square destination = getSquare(shift.destination);
        if (!destination.isAvailable(figure.color)) {
            return false;
        }

        Figure destinationFigure = destination.figure;
        if (destinationFigure == null) {
            return true;
        }

        /*
            If destination cell isn't empty,
            only Scarab can be swapped with Pyramid and Anubis.
         */
        return figure instanceof Scarab
                && (destinationFigure instanceof Pyramid || destinationFigure instanceof Anubis)
                && origin.isAvailable(destinationFigure.color);
    }


    /**
     * This method check all possible moves of all my figures, and return available.
     */
    public List<Move> getAvailableMoves(Color myColor) {
        List<Move> list = new ArrayList<>();

        for (int x = 0; x < getWidth(); x++) {
            for (int y = 0; y < getHeight(); y++) {
                BoardCoordinate origin = new BoardCoordinate(x, y);
                Figure figure = getSquare(origin).figure;
                if (figure != null && figure.color == myColor) {
                    list.add(new Rotate(origin, CLOCKWISE));
                    list.add(new Rotate(origin, COUNTERCLOCKWISE));

                    for (int dx = -1; dx <= 1; dx++) {
                        for (int dy = -1; dy <= 1; dy++) {
                            Shift move = new Shift(origin, new BoardCoordinate(origin.getX() + dx, origin.getY() + dy));
                            if (isMoveValid(move, myColor)) {
                                list.add(move);
                            }
                        }
                    }
                }
            }
        }
        return list;
    }

    public Figure makeMove2(Move move, Color myColor) {
        Figure currFigure = null;
        if (move instanceof Rotate) {
            Rotate rotate = (Rotate) move;
            if (!isMoveValid(rotate, myColor)) {
                throw new RuntimeException(String.format("Not valid move:%s", rotate));
            }
            currFigure = getSquare(rotate.getOrigin()).figure;
            getSquare(rotate.getOrigin()).figure.doTurn(rotate.getRotation());

        } else {
            Shift shift = (Shift) move;
            if (!isMoveValid(shift, myColor)) {
                throw new RuntimeException(String.format("Not valid move:%s", shift));
            }
            Square origin = getSquare(shift.origin);
            Square destination = getSquare(shift.destination);
        /*
            We swap figures from origin and destination. If destination cell is empty, we swap origin figure with null.
         */
            Figure temp = origin.figure;
            currFigure = temp;
            origin.figure = destination.figure;
            destination.figure = temp;
        }

        return currFigure;
    }

    /**
     * This method apply move figure rotation, and change board state.
     */
    public void makeMove(Move move, Color myColor) {
        if (move instanceof Rotate) {
            Rotate rotate = (Rotate) move;
            if (!isMoveValid(rotate, myColor)) {
                throw new RuntimeException(String.format("Not valid move:%s", rotate));
            }
            getSquare(rotate.getOrigin()).figure.doTurn(rotate.getRotation());

        } else {
            Shift shift = (Shift) move;
            if (!isMoveValid(shift, myColor)) {
                throw new RuntimeException(String.format("Not valid move:%s", shift));
            }
            Square origin = getSquare(shift.origin);
            Square destination = getSquare(shift.destination);
        /*
            We swap figures from origin and destination. If destination cell is empty, we swap origin figure with null.
         */
            Figure temp = origin.figure;
            origin.figure = destination.figure;
            destination.figure = temp;
        }
    }

    /**
     * This method is simply 100 times faster than clone(). But you can use clone() if you like it.
     *
     * @return copy of board
     */
    public MyBoard clone() {
        MyBoard newBoard = new MyBoard();
        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {
                Figure newFigure = null;
                if (squares[y][x].figure != null) {
                    newFigure = copyFigure(squares[y][x].figure);
                }

                Square newSquare = new Square(squares[y][x].color);
                newSquare.figure = newFigure;

                newBoard.squares[y][x] = newSquare;
            }
        }

        return newBoard;
    }

    private Figure copyFigure(Figure figure) {

        if (figure instanceof Anubis) {
            return new Anubis(figure.getOrientation(), figure.getColor());
        } else if (figure instanceof Pharaoh) {
            return new Pharaoh(figure.getOrientation(), figure.getColor());
        } else if (figure instanceof Pyramid) {
            return new Pyramid(figure.getOrientation(), figure.getColor());
        } else if (figure instanceof Scarab) {
            return new Scarab(figure.getOrientation(), figure.getColor());
        } else if (figure instanceof Sphinx) {
            return new Sphinx(figure.getOrientation(), figure.getColor());
        }

        return null;
    }

    public String getNotation() {
        final StringBuilder notation = new StringBuilder();
        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {
                Figure figure = squares[y][x].figure;
                if (figure != null) {
                    notation.append(getFigureCode(figure, figure.getColor()));
                    if (!(figure instanceof Pharaoh)) {
                        notation.append(figure.getOrientation());
                    }
                    notation.append(" ").append(y).append(x).append(", ");
                }

            }
        }

        return notation.toString();
    }

    private String getFigureCode(Figure figure, Color color) {
        String code = "";
        if (figure instanceof Anubis) {
            code = "a";
        } else if (figure instanceof Pyramid) {
            code = "p";
        } else if (figure instanceof Scarab) {
            code = "c";
        } else if (figure instanceof Sphinx) {
            code = "s";
        } else if (figure instanceof Pharaoh) {
            code = "ph";
        }

        if (color == Color.WHITE) {
            code = code.toUpperCase();
        }

        return code;
    }

    public int getFigureCount(Color color) {
        int counter = 0;
        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {
                Figure figure = squares[y][x].figure;
                if (figure != null && figure.color == color) {
                    counter++;
                }
            }
        }

        return counter;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
